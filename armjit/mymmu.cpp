﻿#include "mymmu.h"

namespace armjit {
	MmuAccessInfo MyMMU::bus_read(int address)
	{
		return MmuAccessInfo(reinterpret_cast<intptr_t>(&testmem[address]), 4);
	}

	MmuAccessInfo MyMMU::bus_readh(int address)
	{
		return MmuAccessInfo(reinterpret_cast<intptr_t>(&testmem[address]), 2);
	}

	MmuAccessInfo MyMMU::bus_readb(int address)
	{
		return MmuAccessInfo(reinterpret_cast<intptr_t>(&testmem[address]), 1);
	}

	MmuAccessInfo MyMMU::bus_write(int address)
	{
		return MmuAccessInfo(reinterpret_cast<intptr_t>(&testmem[address]), 4);
	}

	MmuAccessInfo MyMMU::bus_writeh(int address)
	{
		return MmuAccessInfo(reinterpret_cast<intptr_t>(&testmem[address]), 2);
	}

	MmuAccessInfo MyMMU::bus_writeb(int address)
	{
		return MmuAccessInfo(reinterpret_cast<intptr_t>(&testmem[address]), 1);
	}
}