﻿#include "armjit.h"

#define THUMB (cpsr & 0b100000) == 0b100000

#define FLAG_SIGN     0x80000000
#define FLAG_ZERO     0x40000000
#define FLAG_CARRY    0x20000000
#define FLAG_OVERFLOW 0x10000000

// !! NIEMALS !! MEHR ALS EINE TRANS_* OPERATION HINTEREINANDER!! OR / AND ZERSTÖREN DIE FLAGS!!

/* // Translates the host cpu's sign flag to the arm's cpsr
#define trans_sign() {\
	Label set_sign = a.newLabel();\
	Label done_sign = a.newLabel();\
	a.js(set_sign);\
	a.and_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), ~(FLAG_SIGN));\
	a.jmp(done_sign);\
	a.bind(set_sign);\
	a.or_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), FLAG_SIGN);\
	a.bind(done_sign);\
}

// Translates the host cpu's zero flag to the arm's cpsr
#define trans_zero() {\
	Label set_zero = a.newLabel();\
	Label done_zero = a.newLabel();\
	a.jz(set_zero);\
	a.and_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), ~(FLAG_ZERO));\
	a.jmp(done_zero);\
	a.bind(set_zero);\
	a.or_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), FLAG_ZERO);\
	a.bind(done_zero);\
}*/

// Translates the host cpu's carry flag to the arm's cpsr
#define trans_carry() {\
	Label set_carry = a.newLabel();\
	Label done_carry = a.newLabel();\
	a.jc(set_carry);\
	a.and_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), ~(FLAG_CARRY));\
	a.jmp(done_carry);\
	a.bind(set_carry);\
	a.or_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), FLAG_CARRY);\
	a.bind(done_carry);\
}

// Translates the NOT host cpu's carry flag to the arm's cpsr
#define trans_borrow() {\
	Label set_carry = a.newLabel();\
	Label done_carry = a.newLabel();\
	a.jnc(set_carry);\
	a.and_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), ~(FLAG_CARRY));\
	a.jmp(done_carry);\
	a.bind(set_carry);\
	a.or_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), FLAG_CARRY);\
	a.bind(done_carry);\
}

// Updates the cpsr's sign flag depending on the value of reg
#define update_sign(reg) {\
	Label set_sign = a.newLabel();\
	Label done_sign = a.newLabel();\
	a.test((reg), 0x80000000);\
	a.jnz(set_sign);\
	a.and_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), ~(FLAG_SIGN));\
	a.jmp(done_sign);\
	a.bind(set_sign);\
	a.or_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), FLAG_SIGN);\
	a.bind(done_sign);\
}

// Updates the cpsr's zero flag depending on the value of reg
#define update_zero(reg) {\
	Label set_zero = a.newLabel();\
	Label done_zero = a.newLabel();\
	a.cmp((reg), 0x0);\
	a.jz(set_zero);\
	a.and_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), ~(FLAG_ZERO));\
	a.jmp(done_zero);\
	a.bind(set_zero);\
	a.or_(dword_ptr_abs(reinterpret_cast<Ptr>(&cpsr), 0), FLAG_ZERO);\
	a.bind(done_zero);\
}

// Updates the cpsr's overflow flag depending on the result and operators of the addition
// NOTE: This algorithm does not preserve the edx register.
// 		 Also optimize a bit maybe?
#define update_overflow_add(result, op, input) {\
	Label overflow_ok_1 = a.newLabel();\
	Label overflow_clear = a.newLabel();\
	Label overflow_end = a.newLabel();\
	a.xor_((op), (input));\
	a.and_((op), 0x80000000);\
	a.jz(overflow_ok_1);\
	a.jmp(overflow_clear);\
	a.bind(overflow_ok_1);\
	a.mov(edx, (result));\
	a.xor_(edx, (input));\
	a.and_(edx, 0x80000000);\
	a.jz(overflow_clear);\
	a.or_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), FLAG_OVERFLOW);\
	a.jmp(overflow_end);\
	a.bind(overflow_clear);\
	a.and_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), ~(FLAG_OVERFLOW));\
	a.bind(overflow_end);\
}

// Updates the cpsr's overflow flag depending on the result and operators of the subtraction
// NOTE: This algorithm does not preserve the edx register.
// 		 Also optimize a bit maybe?
#define update_overflow_sub(result, op, input) {\
	Label overflow_ok_1 = a.newLabel();\
	Label overflow_clear = a.newLabel();\
	Label overflow_end = a.newLabel();\
	a.xor_((op), (input));\
	a.and_((op), 0x80000000);\
	a.jnz(overflow_ok_1);\
	a.jmp(overflow_clear);\
	a.bind(overflow_ok_1);\
	a.mov(edx, (result));\
	a.xor_(edx, (input));\
	a.and_(edx, 0x80000000);\
	a.jnz(overflow_clear);\
	a.or_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), FLAG_OVERFLOW);\
	a.jmp(overflow_end);\
	a.bind(overflow_clear);\
	a.and_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), ~(FLAG_OVERFLOW));\
	a.bind(overflow_end);\
}

namespace armjit {
	
	void ARM::jit()
	{
		JitBlock* block = NULL;

		// Lookup block (address, hash...)
		// ...      
			                                                                                    
		if (block == NULL) { // Translate
			block = new JitBlock;

			if (THUMB) {
				this->translate_thumb(block);
			} else {
				this->translate_arm(block);
			}
			
			this->_blocks.push_back(*block);
		}

		if (THUMB) this->r15 += 4;
			else this->r15 += 8;   

		//r2 = 0xFFFFFFFF;
		r0 = 0x7FFFFFFF;
		r1 = 1;
		block->func();
		cout << endl << "== Registers ==" << endl; 
		for (int i = 0; i < 16; i++)
			cout << "r" << i << ": 0x" << std::hex << *gprs[i] << std::dec << endl;
		cout << "cpsr: 0x" << std::hex << cpsr << std::dec << endl;
	}

	void ARM::translate_arm(JitBlock* block)
	{
	}

	void ARM::translate_thumb(JitBlock* block)
	{
		X86Assembler a(&rt);
		FileLogger logger(stderr);
		bool translate = true;
		uint32_t ptr = r15;
	
 		a.setLogger(&logger);

		block->offset = ptr;
		block->thumb = true;
		
		a.pusha();

		while (translate)
		{
			uint16_t op = mmu->readh(ptr);

			if ((op & 0xF800) < 0x1800) { // Move shifted register
				int rd = op & 7;
				int rs = (op >> 3) & 7;
				int offset = (op >> 6) & 0x1f;
				a.mov(eax, dword_ptr_abs(asmjit_cast<Ptr>(gprs[rs]), 0)); // mov eax, [rs]
				if (offset != 0)
				{
					switch ((op >> 11) & 3)
					{
					case 0: // LSL
						a.shl(eax, offset); // shl eax, offset
						break;
					case 1: // LSR
						a.shr(eax, offset); // shr eax, offset
						break;
					case 2: // ASR
						a.sar(eax, offset); // sar eax, offset
						break;
					}
					trans_carry();
				}
				update_sign(eax);
				update_zero(eax);
				a.mov(dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0), eax); // mov [rd], eax
			} else if ((op & 0xF800) == 0x1800) { // Add / Subtract
				int rd = op & 7;
				int rs = (op >> 3) & 7;
				int val3 = (op >> 6) & 7; // can be either interpreted as register or as immediate value depending on I bit.
				a.mov(eax, dword_ptr_abs(asmjit_cast<Ptr>(gprs[rs]), 0)); // mov eax, [rs]
				a.mov(ebx, eax); // mov ebx, eax
				switch ((op >> 9) & 3)
				{
				case 0: // ADD rd, rs, rn
					a.add(eax, dword_ptr_abs(asmjit_cast<Ptr>(gprs[val3]), 0)); // add eax, [rn]
					trans_carry();
					update_overflow_add(eax, ebx, dword_ptr_abs(asmjit_cast<Ptr>(gprs[val3]), 0));
					break;
				case 1: // SUB rd, rs, rn
					a.sub(eax, dword_ptr_abs(asmjit_cast<Ptr>(gprs[val3]), 0)); // sub eax, [rn]
					trans_borrow();
					update_overflow_sub(eax, ebx, dword_ptr_abs(asmjit_cast<Ptr>(gprs[val3]), 0));
					break;
				case 2: // ADD rd, rs, #imm3
					a.add(eax, val3); // add eax, imm3
					trans_carry();
					update_overflow_add(eax, ebx, val3);
					break;
				case 3: // SUB rd, rs, #imm3
					a.sub(eax, val3); // sub eax, imm3
					trans_borrow();
					update_overflow_sub(eax, ebx, val3);
					break;
				}
				update_sign(eax);
				update_zero(eax);
				a.mov(dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0), eax);
			} else if ((op & 0xE000) == 0x2000) { // Move / compare / add / subtract immediate
				int offset = op & 0xFF;
				int rd = (op >> 8) & 7;
				switch ((op >> 11) & 3)
				{
				case 0: // MOV
					a.mov(dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0), offset); // mov [rd], offset 
					// SUPA DUPA OPTIMIZED FLAG ALTERING
					if (offset == 0) a.or_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), FLAG_ZERO);
						else a.and_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), ~(FLAG_ZERO));
					if (offset & 0x80000000) a.or_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), FLAG_SIGN);
						else a.and_(dword_ptr_abs(asmjit_cast<Ptr>(&cpsr), 0), ~(FLAG_SIGN));
					break;
				case 1: // CMP
					a.mov(eax, dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0)); // mov eax, [rd]
					a.mov(ebx, eax); // mov ebx, eax
					a.sub(eax, offset); // sub eax, offset
					trans_borrow();
					update_overflow_sub(eax, ebx, offset);
					update_sign(eax);
					update_zero(eax);
					break;
				case 2: // ADD
					a.mov(eax, dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0)); // mov eax, [rd]
					a.mov(ebx, eax); // mov ebx, eax
					a.add(eax, offset); // add eax, offset
					trans_carry();
					update_overflow_add(eax, ebx, offset);
					update_sign(eax);
					update_zero(eax);
					a.mov(dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0), eax);
					break;
				case 3: // SUB
					a.mov(eax, dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0)); // mov eax, [rd]
					a.mov(ebx, eax); // mov ebx, eax
					a.sub(eax, offset); // sub eax, offset
					trans_borrow();
					update_overflow_sub(eax, ebx, offset);
					update_sign(eax);
					update_zero(eax);
					a.mov(dword_ptr_abs(asmjit_cast<Ptr>(gprs[rd]), 0), eax); // mov [rd], eax
					break;
				}
			} else if ((op & 0xFC00) == 0x4000) { // ALU operations
				int rd = op & 7;
				int rs = (op >> 3) & 7;
				switch ((op >> 6) & 0xF)
				{
				}
			}
					
			a.add(dword_ptr_abs(asmjit_cast<Ptr>(gprs[15]), 0), 2); // increment pc	
			translate = false;
			ptr += 2;
		}
		
		a.popa();
		a.ret();

		block->length = ptr - r15;
		block->func = asmjit_cast<JitFunc>(a.make());
	}

}