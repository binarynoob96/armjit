﻿#ifndef _MMU_H_
#define _MMU_H_

#include <iostream>
#include <cstdint>
#include <stdexcept>

#define WORDALIGN(n) ((n) & 0xFFFFFFFC)
#define HWORDALIGN(n) ((n) & 0xFFFFFFFE)

namespace armjit {
	class MmuAccessInfo {
	public:
		intptr_t address; // the actual (translated) address
		int width; // width of the data bus in bytes
		MmuAccessInfo();
		MmuAccessInfo(intptr_t address, int width);
	};

	class MMU {
	public:
		virtual MmuAccessInfo bus_read(int address) {} ;
		virtual MmuAccessInfo bus_readh(int address) {} ;
		virtual MmuAccessInfo bus_readb(int address) {} ;
		virtual MmuAccessInfo bus_write(int address) {} ;
		virtual MmuAccessInfo bus_writeh(int address) {} ;
		virtual MmuAccessInfo bus_writeb(int address) {} ;
		int read(int address);
		int readh(int address);
		int readb(int address);
		void write(int address, int value);
		void writeh(int address, int value);
		void writeb(int address, int value);
	};
}

#endif
