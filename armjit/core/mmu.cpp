﻿#include "mmu.h"

namespace armjit {
	
	MmuAccessInfo::MmuAccessInfo() {}

	MmuAccessInfo::MmuAccessInfo(intptr_t address, int width)
	{
		this->address = address;
		this->width = width;
	}

	int MMU::read(int address)
	{
		MmuAccessInfo mai = this->bus_read(WORDALIGN(address));
		switch (mai.width)
		{
		case 4: return *reinterpret_cast<uint32_t*>(mai.address);
		case 2: return this->readh(address);
		case 1: return this->readb(address);
		default:
			throw std::runtime_error("core/mmu.cpp: Invalid data bus width for mmu access.");
		}
	}

	int MMU::readh(int address)
	{
		MmuAccessInfo mai = this->bus_readh(HWORDALIGN(address));
		switch (mai.width)
		{
		case 4: return this->read(address);
		case 2: return *reinterpret_cast<uint16_t*>(mai.address);
		case 1: return this->readb(address);
		default:
			throw std::runtime_error("core/mmu.cpp: Invalid data bus width for mmu access.");
		}
	}

	int MMU::readb(int address)
	{
		MmuAccessInfo mai = this->bus_readb(address);
		switch (mai.width)
		{
		case 4: return this->read(address);
		case 2: return this->readh(address);
		case 1: return *reinterpret_cast<uint8_t*>(mai.address);
		default:
			throw std::runtime_error("core/mmu.cpp: Invalid data bus width for mmu access.");
		}
	}

	void MMU::write(int address, int value)
	{
		MmuAccessInfo mai = this->bus_write(WORDALIGN(address));
		switch (mai.width)
		{
		case 4: *reinterpret_cast<uint32_t*>(mai.address) = value; break;
		case 2: this->writeh(address, value & 0xFFFF); break;
		case 1: this->writeb(address, value & 0xFF); break;
		default:
			throw std::runtime_error("core/mmu.cpp: Invalid data bus width for mmu access.");
		}
	}

	void MMU::writeh(int address, int value)
	{
		MmuAccessInfo mai = this->bus_writeh(HWORDALIGN(address));
		switch (mai.width)
		{
		case 4: this->write(address, (value << 16) | value); break;
		case 2: *reinterpret_cast<uint16_t*>(mai.address) = value & 0xFFFF; break;
		case 1: this->writeb(address, value & 0xFF); break;
		default:
			throw std::runtime_error("core/mmu.cpp: Invalid data bus width for mmu access.");
		}
	}

	void MMU::writeb(int address, int value)
	{
		MmuAccessInfo mai = this->bus_writeb(address);
		switch (mai.width)
		{
		case 4: this->write(address, (value << 24) | (value << 16) | (value << 8) | value); break;
		case 2: this->writeh(address, (value << 8) | value); break;
		case 1: *reinterpret_cast<uint8_t*>(mai.address) = value & 0xFF; break;
		default:
			throw std::runtime_error("core/mmu.cpp: Invalid data bus width for mmu access.");
		}
	}

}
