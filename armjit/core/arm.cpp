﻿#include "armjit.h"

#define reg(n) *gprs[n]

namespace armjit {

	ARM::ARM(MMU* mmu)
	{
		// Store mmu
		this->mmu = mmu;

		// Setup registers
		this->gprs[0] = &r0;
		this->gprs[1] = &r1;
		this->gprs[2] = &r2;
		this->gprs[3] = &r3;
		this->gprs[4] = &r4;
		this->gprs[5] = &r5;
		this->gprs[6] = &r6;
		this->gprs[7] = &r7;
		this->gprs[15] = &r15;
		this->cpsr = 0x1F;
		this->cpsr |= 0b100000;
		this->update_gprs();

		for (int i = 0; i < 16; i++)
        	*gprs[i] = 0;
	}

	void ARM::update_gprs()
	{
		switch (cpsr & 0x1F) // which mode?
	    {
	    case 0x10: // user
	        gprs[8] = &r8;
	        gprs[9] = &r9;
	        gprs[10] = &r10;
	        gprs[11] = &r11;
	        gprs[12] = &r12;
	        gprs[13] = &r13;
	        gprs[14] = &r14;
	        break;
	    case 0x11: // fiq
	        gprs[8] = &r8_fiq;
	        gprs[9] = &r9_fiq;
	        gprs[10] = &r10_fiq;
	        gprs[11] = &r11_fiq;
	        gprs[12] = &r12_fiq;
	        gprs[13] = &r13_fiq;
	        gprs[14] = &r14_fiq;
	        break;
	    case 0x12: // irq
	        gprs[8] = &r8;
	        gprs[9] = &r9;
	        gprs[10] = &r10;
	        gprs[11] = &r11;
	        gprs[12] = &r12;
	        gprs[13] = &r13_irq;
	        gprs[14] = &r14_irq;
	        break;
	    case 0x13: // supervisor (swi)
	        gprs[8] = &r8;
	        gprs[9] = &r9;
	        gprs[10] = &r10;
	        gprs[11] = &r11;
	        gprs[12] = &r12;
	        gprs[13] = &r13_svc;
	        gprs[14] = &r14_svc;
	        break;
	    case 0x17: // abort
	        gprs[8] = &r8;
	        gprs[9] = &r9;
	        gprs[10] = &r10;
	        gprs[11] = &r11;
	        gprs[12] = &r12;
	        gprs[13] = &r13_abt;
	        gprs[14] = &r14_abt;
	        break;
	    case 0x1B: // undefined
	        gprs[8] = &r8;
	        gprs[9] = &r9;
	        gprs[10] = &r10;
	        gprs[11] = &r11;
	        gprs[12] = &r12;
	        gprs[13] = &r13_und;
	        gprs[14] = &r14_und;
	        break;
	    case 0x1F: // system
	        gprs[8] = &r8;
	        gprs[9] = &r9;
	        gprs[10] = &r10;
	        gprs[11] = &r11;
	        gprs[12] = &r12;
	        gprs[13] = &r13;
	        gprs[14] = &r14;
	        break;
	    default:
	        throw std::runtime_error("core/arm.cpp:94: Invalid cpu mode encountered.");
	    }
	}
}