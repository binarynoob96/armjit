﻿#ifndef _BLOCK_H_
#define _BLOCK_H_

#include "../asmjit/asmjit.h"

using namespace asmjit;

typedef void (*JitFunc)();

namespace armjit {
	class JitBlock {
	public:
		JitFunc func;
		int offset;
		int length;
		int crc32;
		bool thumb;
	};
}

#endif