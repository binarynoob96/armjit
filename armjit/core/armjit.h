﻿#ifndef _JIT_H_
#define _JIT_H_

#include <iostream>
#include <cstdint>
#include <list>
#include <stdexcept>
#include "../asmjit/asmjit.h"
#include "block.h"
#include "mmu.h"

using namespace std;
using namespace asmjit;
using namespace asmjit::x86;

namespace armjit {
	class ARM {
		// JIT Stuff
		JitRuntime rt;
		list<JitBlock> _blocks;

		// Memory
		MMU* mmu;

		// ARM registers
		uint32_t* gprs[16];
		uint32_t r0;
		uint32_t r1;
		uint32_t r2;
		uint32_t r3;
		uint32_t r4;
		uint32_t r5;
		uint32_t r6;
		uint32_t r7;
		uint32_t r8;
		uint32_t r9;
		uint32_t r10;
		uint32_t r11;
		uint32_t r12;
		uint32_t r13;
		uint32_t r14;
		uint32_t r15;
		uint32_t r8_fiq;
		uint32_t r9_fiq;
		uint32_t r10_fiq;
		uint32_t r11_fiq;
		uint32_t r12_fiq;
		uint32_t r13_fiq;
		uint32_t r14_fiq;
		uint32_t r13_svc;
		uint32_t r14_svc;
		uint32_t r13_abt;
		uint32_t r14_abt;
		uint32_t r13_irq;
		uint32_t r14_irq;
		uint32_t r13_und;
		uint32_t r14_und;
		uint32_t cpsr;
		uint32_t spsr_fiq;
		uint32_t spsr_svc;
		uint32_t spsr_abt;
		uint32_t spsr_irq;
		uint32_t spsr_und;
	public:
		ARM(MMU* mmu);
		void update_gprs();
		void jit();
		void translate_arm(JitBlock* block);
		void translate_thumb(JitBlock* block);
	};
}

#endif
