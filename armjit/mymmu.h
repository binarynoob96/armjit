﻿#include "core/mmu.h"

namespace armjit {
	class MyMMU : public MMU {
		uint8_t testmem[0x100000];
	public:
		MmuAccessInfo bus_read(int address) override;
		MmuAccessInfo bus_readh(int address) override;
		MmuAccessInfo bus_readb(int address) override;
		MmuAccessInfo bus_write(int address) override;
		MmuAccessInfo bus_writeh(int address) override;
		MmuAccessInfo bus_writeb(int address) override;
	};
}
