﻿#include <iostream>
#include "asmjit/asmjit.h"
#include "core/armjit.h"
#include "mymmu.h"

using namespace std;
using namespace asmjit;
using namespace asmjit::x86;
using namespace armjit;

int main (int argc, char *argv[])
{
	MyMMU mmu;
	ARM arm(&mmu);

	mmu.writeh(0, 0b0001100001000010);
	arm.jit();

	return 0;
}

